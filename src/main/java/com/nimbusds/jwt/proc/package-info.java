/**
 * Secure framework for application-specific processing of JSON Web Tokens
 * (JWTs).
 *
 * <p>References:
 *
 * <ul>
 *     <li><a href="http://tools.ietf.org/html/rfc7519">RFC 7519 (JWT)</a>
 * </ul>
 */
package com.nimbusds.jwt.proc;